﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 角色控制輸入類
/// </summary>
[RequireComponent(typeof(Player))]
public class PlayerInput : MonoBehaviour
{
    private Player player;
    private Camera mainCamera;
    private int width, height;

    private void Start()
    {
        player = GetComponent<Player>();
        mainCamera = CameraController.Instance.GetMainCamera();
        width = mainCamera.pixelWidth;
        height = mainCamera.pixelHeight;
    }

    private void Update()
    {
        PlayerMoveInput();
    }

    // Move Direction
    private void PlayerMoveInput()
    {
        // Keep Press
        if (Input.GetMouseButton(0))
        {
            player.isHoldTap = true;

            Vector3 screenPos = Input.mousePosition;
            float camZ_Pos = mainCamera.transform.localPosition.z;
            screenPos.z = 12.5f;

            var center_x = Screen.width / 2;
            var center_y = Screen.height / 2;
            var dis_x = screenPos.x - center_x;
            var dis_y = screenPos.y - center_y;

            var rad = Mathf.Atan2(dis_y, dis_x);
            var v_x = -Mathf.Cos(rad);
            var v_y = -Mathf.Sin(rad);
            var moveDir = new Vector2(v_x, v_y);

            player.OnMoveDirection(moveDir);

            //Debug.Log(moveDir);
        }

        if (Input.GetMouseButtonUp(0))
        {
            player.isHoldTap = false;

            player.OnMoveDirection(Vector2.zero);
        }
    }
}
