﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 角色動作類
/// </summary>
public class CharacterBehavior : MonoBehaviour
{   
    public void SetCharacterFaceDirector(int _faceDir)
    {
        this.transform.localEulerAngles = _faceDir == 1
            ? new Vector3(0, 0, 0) 
            : new Vector3(0, 180, 0);
    }
}
