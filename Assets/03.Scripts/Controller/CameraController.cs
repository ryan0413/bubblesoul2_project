﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : SingletonManager<CameraController>
{
    [SerializeField] private Camera m_MainCamera;
    public Camera GetMainCamera()
    {
        if (m_MainCamera != null)
            return m_MainCamera;
        else
        {
            Debug.Log("no camera assign");
            return null;
        }            
    }

    [SerializeField] private Transform m_CameraOffset;

    private Player player;
    public void AssignPlayerRef(Player _player) { player = _player; }

    private void Awake()
    {

    }

    private void Start()
    {

    }

    private void Update()
    {
        
    }

    private void LateUpdate()
    {
        if (player != null)
        {
            FollowPlayerPosition();
        }        
    }

    /// <summary>
    /// 漸進跟隨
    /// </summary>
    private void FollowPlayerPosition()
    {
        float distance = Vector3.Distance(this.transform.position, player.GetPlayerPosition());
        if (distance <= 0.01f)
        {
            return;
        }
        else
        {
            this.transform.position = Vector3.Lerp(this.transform.position, player.GetPlayerPosition(), distance * 10 * Time.deltaTime);
        }
    }
}
