﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 角色狀態類
/// </summary>
[RequireComponent(typeof(Controller2D))]
public class Player : MonoBehaviour
{
    [SerializeField] private CharacterBehavior characterBehavior;

    private float accelerationTimeAirborne = 0.2f;
    private float accelerationTimeGrounded = 0.1f;

    private float currentMoveSpeed;
    public float GetMoveSpeed() { return currentMoveSpeed; }
    public void SetMoveSpeed(float value) { currentMoveSpeed = value; }
    public void ReverseMoveSpeed() { currentMoveSpeed *= -1; }

    public bool isReverseGravity = false; // 反轉重力
    private float currentGravity;
    private float normalGravity = -4.9f;
    private float inWaterGravity = -3.25f;
    public float Gravity {
        get { return isReverseGravity == false ? currentGravity : currentGravity * -1; }
        set { currentGravity = value; }
    }

    private Vector3 velocity;
    private float velocityXSmoothing;
    private float velocityYSmoothing;
    private Controller2D controller;
    private Vector2 directionalInput;

    [HideInInspector] public bool isHoldTap = false; // 持續按壓    

    [Space(10)]
    [Header("Attribute")]
    private bool isFreezePlayer = true;
    public void SetPlayerFreeze(bool _isFreeze) { isFreezePlayer = _isFreeze; }
    private bool isInBuoyancyArea = false; // 水中
    private bool isInAttractionArea = false; // 引力

    [Space(10)]
    [Header("PlayerEffect")]
    public GameObject m_StartPowerEffect;
    public GameObject eatCoinPrefab; // 吃金幣效果
    public GameObject moveTrail; // 拖曳特效

    [Header("Player")]
    [SerializeField] private PlayerData m_PlayerData;
    private int m_PlayerHealth = 3;
    private int m_Stamina = 100;
    private float m_MoveSpeed = 4;
    private float m_GravityScale = 1;

    public void InitPlayerDatas()
    {
        controller = GetComponent<Controller2D>();

        Gravity = normalGravity;

        m_PlayerHealth = m_PlayerData.m_PlayerHealth;
        m_Stamina = m_PlayerData.m_Stamina;
        m_MoveSpeed = m_PlayerData.m_MoveSpeed;
        currentMoveSpeed = m_MoveSpeed;
        m_GravityScale = m_PlayerData.m_GravityScale;
    }

    private void Awake()
    {
        
    }

    private void Start()
    {
        
    }

    public void StartPowerUp(Vector3 _startPoint, Vector3 _endPoint)
    {
        if (isFreezePlayer == true)
        {
            StartCoroutine(controller.StartPowerUp(_startPoint, _endPoint));
        }
    }

    private void Update()
    {
        if (isFreezePlayer == true)
        {
            velocity = Vector3.zero;
            directionalInput = Vector2.zero;
            return;
        }

        // 速度計算
        CalculateVelocity();

        // 角色移動
        controller.Move(velocity * Time.deltaTime, directionalInput);

        if (controller.collisions.above || controller.collisions.below)
        {
            if (controller.collisions.slidingDownMaxSlope)
            {
                velocity.y += controller.collisions.slopeNormal.y * -Gravity * Time.deltaTime;
            }
            else
            {
                velocity.y = 0;
            }
        }

        characterBehavior.SetCharacterFaceDirector(controller.collisions.faceDir); // 角色面向
    }

    public void OnMoveDirection(Vector2 input)
    {
        directionalInput = input;
    }

    private void CalculateVelocity()
    {
        float targetVelocityX = directionalInput.x * GetMoveSpeed();
        float targetVelocityY = directionalInput.y * GetMoveSpeed();

        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);

        if (isHoldTap == true)
        {
            velocity.y = Mathf.SmoothDamp(velocity.y, targetVelocityY, ref velocityYSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
        }
        else
        {
            if (isInAttractionArea == false)
            {
                velocity.y += Gravity * Time.deltaTime;
            }            
        }
    }

    public Vector3 GetPlayerPosition()
    {
        return this.transform.position;
    }

    public void SetStartDashPower(bool _enable)
    {
        m_StartPowerEffect.SetActive(_enable);
    }

    #region 碰撞偵測
    //碰撞偵測
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.CompareTag("Item"))
        {
            if (other.gameObject.GetComponent<Coin>() != null)
            {
                CheckPlayerCoinScore(other.gameObject.GetComponent<Coin>(), other.gameObject, eatCoinPrefab, false);
            }

            if (other.gameObject.GetComponent<Hurt>() != null)
            {
                CheckPlayerHealth(other.gameObject.GetComponent<Hurt>());
            }

            switch (other.transform.name)
            {
                case "SpeedItem"://衝氣波
                    StartCoroutine(other.gameObject.GetComponent<Speed>().SpeedUp(this));
                    break;
                case "AirFlowItem"://氣流通道
                    StartCoroutine(other.gameObject.GetComponent<AirFlow>().AirPathFollow(this));
                    break;
                case "PortalItem"://傳送門
                    StartCoroutine(other.gameObject.GetComponent<Portal>().StartPortal(this));
                    break;
                case "CloseDoorItem"://單次關閉門
                    StartCoroutine(other.gameObject.GetComponent<CloseDoor>().StartCloseDoor(this));
                    break;

                case "Grass"://草堆
                    other.gameObject.SetActive(false);
                    break;

                case "Finish"://終點
                    GameManager.Instance.m_GamePlayStat = GameManager.GamePlayStat.Finish;
                    GameManager.Instance.isFinishTiming = true;
                    break;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.transform.CompareTag("Item"))
        {
            switch (other.transform.name)
            {
                case "BuoyancyItem": // 水域
                    isInBuoyancyArea = true;
                    isReverseGravity = true;
                    SetMoveSpeed(2);
                    currentGravity = inWaterGravity;
                    break;
                case "AttractionItem": // 吸引力
                    isInAttractionArea = true;
                    SetMoveSpeed(1.5f);
                    if (isHoldTap == false)
                    {
                        Vector3 heading = other.bounds.center - this.transform.position;
                        float distance = heading.magnitude;
                        Vector3 direction = heading / distance; // normalized direction.
                        controller.Move(direction * Time.deltaTime * 5f);
                    }
                    break;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.CompareTag("Item"))
        {
            switch (other.transform.name)
            {
                case "BuoyancyItem": // 水域
                    isInBuoyancyArea = false;
                    isReverseGravity = false;
                    SetMoveSpeed(4);
                    currentGravity = normalGravity;
                    break;
                case "AttractionItem": // 吸引力
                    isInAttractionArea = false;
                    SetMoveSpeed(4);
                    break;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Item"))
        {
            if (collision.gameObject.GetComponent<Coin>() != null)
            {
                CheckPlayerCoinScore(collision.gameObject.GetComponent<Coin>(), collision.gameObject, eatCoinPrefab, true);
            }

            //switch (collision.transform.name)
            //{
            //    case "Wood"://木堆
            //        StartCoroutine(collision.gameObject.GetComponent<Wood>().ReboundOnce(m_Rigidbody));
            //        break;
            //    case "Stone"://石堆
            //        StartCoroutine(collision.gameObject.GetComponent<Stone>().ReboundOnce(m_Rigidbody));
            //        break;
            //}

            if (collision.gameObject.GetComponent<Hurt>() != null)
            {
                CheckPlayerHealth(collision.gameObject.GetComponent<Hurt>());
            }
        }
    }
    #endregion



    /// <summary>
    /// 獲取分數
    /// </summary>
    /// <param name="coinClass">目標內容</param>
    /// <param name="target">目標物件</param>
    /// <param name="eatEffect">獲取特效</param>
    /// <param name="enable">是否單次獲取</param>
    private void CheckPlayerCoinScore(Coin coinClass, GameObject target, GameObject eatEffect, bool enable)
    {
        if (eatEffect != null)
        {
            GameObject obj = Instantiate(eatCoinPrefab);
            obj.transform.position = target.transform.position;
        }
        
        GameManager.Instance.AddCoin(coinClass.coinScore);
        target.SetActive(enable);
        //coinClass.enabled = enable;
    }

    private void CheckPlayerHealth(Hurt _hurtClass)
    {
        m_Stamina -= _hurtClass.hurt;
        if (m_Stamina <= 0)
        {
            m_PlayerHealth--;
            m_Stamina = m_PlayerData.m_Stamina;
            if (m_PlayerHealth <= 0)
            {
                //SceneManager.LoadScene("TestControl");
            }
        }
    }
}
