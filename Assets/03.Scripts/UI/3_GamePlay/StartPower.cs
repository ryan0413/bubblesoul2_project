﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartPower : MonoBehaviour
{
    public Image m_Fill;
    public Text m_Text;

    [HideInInspector]
    public float timer = 0.0f;
    [HideInInspector]
    public bool isPutScreen = false;

    private bool isLockPower;
    private float currentFill;

    private void Start()
    {
        ResetPower();
    }

    public void StartDashUp()
    {
        isLockPower = true;
        m_Text.text = "START !!!";
        StopPowerRange(currentFill);
    }

    public void ResetPower()
    {
        isLockPower = false;
        currentFill = 0.0f;
        timer = 0.0f;
        m_Text.text = "KEPP TAP TO POWER UP";
    }

    public void StopPowerRange(float _amount)
    {
        m_Fill.fillAmount = _amount;
    }

    private void Update()
    {
        if (isLockPower == true)
        {
            return;
        }

        if (GameManager.Instance.isPowerUp == true)
        {
            isPutScreen = true;
        }
        else
        {
            isPutScreen = false;
        }

        if (isPutScreen == true)
        {
            timer += Time.deltaTime;
            if (timer > 1)
            {
                timer = 0.0f;
            }            
        }
        else
        {
            timer = 0.0f;
        }
        m_Fill.fillAmount = timer;
        currentFill = m_Fill.fillAmount;
    }
}
