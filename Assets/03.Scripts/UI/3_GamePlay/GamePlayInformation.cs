﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayInformation : MonoBehaviour
{
    [SerializeField] private Text txt_LevelTitle;
    [SerializeField] private Text txt_Time;
    [SerializeField] private Text txt_Score;   

    private float addingTimer;
    private int currentTimer;
    public int CurrentTimer() { return currentTimer; }
    private int currentScore;
    public int CurrentScore() { return currentScore; }

    public void RunTimeUpdate(float _timer)
    {
        addingTimer += _timer;
        if (addingTimer >= 1f)
        {
            currentTimer += 1;
            addingTimer = 0f;
        }
        txt_Time.text = "TIME : " + currentTimer.ToString();
    }

    public void AddCoin(int score)
    {
        currentScore += score;
        txt_Score.text = "SCORE : " + ((int)currentScore).ToString();
    }

    public void ResetInformation()
    {
        addingTimer = 0f;
        currentTimer = 0;
        currentScore = 0;
        txt_LevelTitle.text = GameManager.Instance.GetCurrentLevelDatas().levelTitle;
        txt_Time.text = "TIME : 0";
        txt_Score.text = "SCORE : " + ((int)currentScore).ToString();      
    }
}
