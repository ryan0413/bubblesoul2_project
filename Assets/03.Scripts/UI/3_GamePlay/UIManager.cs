﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// GamePlaying UI
/// </summary>
public class UIManager : MonoBehaviour
{
    [SerializeField] private GamePlayInformation m_GamePlayInformation;
    [SerializeField] private StartPower m_StartPower;
    [SerializeField] private Finish m_Finish;

    private void Awake()
    {
        
    }

    private void Start()
    {
        GameManager.Instance.InitializeGame();
    }

    private void Update()
    {
        if (GameManager.Instance.isFirstTap == false)
        {
            if(GameManager.Instance.m_GamePlayStat == GameManager.GamePlayStat.Playing)
                m_GamePlayInformation.RunTimeUpdate(Time.deltaTime);
        }
    }

    /// <summary>
    /// 集氣持續時間
    /// </summary>
    public float GetStartPowerTimer()
    {
        return m_StartPower.timer;
    }

    public void StartDashUp()
    {
        m_StartPower.StartDashUp();
    }

    public void HidePowerUpUI()
    {
        m_StartPower.gameObject.SetActive(false);
    }

    public void AddCoin(int _score)
    {
        m_GamePlayInformation.AddCoin(_score);
    }

    public void OpenFinishPanel()
    {
        m_Finish.gameObject.SetActive(true);
        m_Finish.ShowFinishDatas(m_GamePlayInformation.CurrentTimer(), m_GamePlayInformation.CurrentScore());
    }

    public void GetLevelFinishInformation(Action<LevelSave> callBack)
    {
        LevelSave currentLevelSave = new LevelSave()
        {
            levelTitle = GameManager.Instance.GetCurrentLevelDatas().levelTitle,
            isDone = true,
            time = m_GamePlayInformation.CurrentTimer(),
            score = m_GamePlayInformation.CurrentScore(),
            totalScore = m_Finish.CurrentTotalScore()
        };
        callBack(currentLevelSave);
    }

    /// <summary>
    /// 初始化關卡介面
    /// </summary>
    public void InitializeGamePlayUI()
    {
        m_GamePlayInformation.gameObject.SetActive(true);
        m_StartPower.gameObject.SetActive(true);
        m_StartPower.ResetPower();
        m_GamePlayInformation.ResetInformation();
        m_Finish.gameObject.SetActive(false);
    }
}
