﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Finish : MonoBehaviour
{
    [SerializeField] private Text txt_Time;
    [SerializeField] private Text txt_Score;
    [SerializeField] private Text txt_TotalScore;

    private int currentTotalScore;
    public int CurrentTotalScore() { return currentTotalScore; }

    public void ShowFinishDatas(int _time, int _score)
    {
        txt_Time.text = "TIME : " + _time.ToString();
        txt_Score.text = "SCORE : " + _score.ToString();
        txt_TotalScore.text = "TOTAL SCORE : " + GetTotalScore(_time, _score);
    }

    private int GetTotalScore(int _time, int _score)
    {
        if (_time == 0)
            return 0;

        currentTotalScore = 1000 + (_score * 10) - (_time * 5);
        if (currentTotalScore <= 0)
            return 0;
        else
            return currentTotalScore;
    }

    #region Button Event
    public void OnPlayAgain()
    {
        GameManager.Instance.OnPlayLevelAgain();
    }

    public void OnMainMenu()
    {
        SceneManager.LoadScene("1_MainMenu");
    }

    public void OnNextLevel()
    {
        GameManager.Instance.OnPlayNextLevel();
    }
    #endregion
}
