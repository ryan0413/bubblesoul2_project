﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AsyncLoading : MonoBehaviour
{
    private float loadingProgress = 0f;
    
    public string nextScene = "2_GamePlay";
    public Image img_FillImage;

    private void Start()
    {
        GameManager.Instance.m_GamePlayStat = GameManager.GamePlayStat.AsyncLoading;
        Debug.Log("開始讀取場景...");
        StartCoroutine(LoadScene(nextScene));
    }

    private IEnumerator LoadScene(string sceneName)
    {
        var asyncScene = SceneManager.LoadSceneAsync(sceneName);
        
        asyncScene.allowSceneActivation = false;

        while (!asyncScene.isDone)
        {
            loadingProgress = Mathf.Clamp01(asyncScene.progress / 0.9f);

            if (asyncScene.progress >= 0.9f)
            {
                loadingProgress = 1;
            }

            img_FillImage.fillAmount = loadingProgress;

            if (img_FillImage.fillAmount >= 0.99f)
            {
                img_FillImage.fillAmount = 1f;
                asyncScene.allowSceneActivation = true;
            }            
            yield return new WaitForEndOfFrame();
        }        
    }
}
