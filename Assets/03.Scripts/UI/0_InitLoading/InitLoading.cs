﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InitLoading : MonoBehaviour
{
    public GameObject m_GameManagerGlobal;
    public float loadingTime = 1f;
    public string nextScene = "1_MainMenu";
    public Image img_FillImage;
    [SerializeField] private Text m_InitLoading;

    private bool isInLoading;
    private const string INIT_LOADING = "Init Loading";
    private string dotString = "";
    private int currentDotNum;
    private float timer = 0.0f;
    private float nextDotTimer = 1f;

    private void Awake()
    {
        GameManager.Instance.m_GamePlayStat = GameManager.GamePlayStat.InitLoading;
        if (m_GameManagerGlobal != null)
        {            
            DontDestroyOnLoad(m_GameManagerGlobal);
        }
    }

    private void Start()
    {
        currentDotNum = 0;
        timer = 0.0f;
        dotString = "";
        m_InitLoading.text = INIT_LOADING;
        isInLoading = true;
        StartCoroutine(StartInitLoading());
        StartCoroutine(LoadingDotAnim());
    }

    private IEnumerator StartInitLoading()
    {        
        img_FillImage.fillAmount = 0f;
        float timer = 0;
        yield return new WaitUntil(() =>
        {
            timer += Time.deltaTime;
            img_FillImage.fillAmount = timer;
            return timer >= loadingTime;
        });
        img_FillImage.fillAmount = 1f;
        yield return new WaitForSeconds(1f);
        isInLoading = true;
        SceneManager.LoadScene(nextScene);
    }
    
    private IEnumerator LoadingDotAnim()
    {
        yield return new WaitUntil(() =>
        {
            timer += 0.1f;
            if (timer >= nextDotTimer)
            {
                if (currentDotNum == 0)
                    dotString = ".";
                else if (currentDotNum == 1)
                    dotString = "..";
                else if (currentDotNum == 2)
                    dotString = "...";

                currentDotNum += 1;

                if (currentDotNum > 2)
                    currentDotNum = 0;

                timer = 0.0f;
            }
            return isInLoading == false;
        });
    }

    private void Update()
    {
        m_InitLoading.text = string.Format("{0}{1}", INIT_LOADING, dotString);
    }
}
