﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Setting : MonoBehaviour
{
    [SerializeField] private GameObject m_ClosePanel;
    [SerializeField] private GameObject m_BtnGroup;

    private Canvas_MainMenu m_Canvas_MainMenu;
    public void SetCanvas_MainMenuRef(Canvas_MainMenu _canvas_MainMenu) { m_Canvas_MainMenu = _canvas_MainMenu; }

    private void Awake()
    {
        
    }

    public void OnOpenSettingPanel(bool _isOpen)
    {
        m_ClosePanel.SetActive(_isOpen);
        m_BtnGroup.SetActive(_isOpen);
    }

    #region Button Event
    // 重置關卡資料
    public void OnDeleteAllLevelDatas()
    {
        m_Canvas_MainMenu.SettingEvent_DeleteAllLevelDatas();        
    }
    #endregion
}
