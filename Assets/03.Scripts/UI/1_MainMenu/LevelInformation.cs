﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelInformation : MonoBehaviour
{
    [SerializeField] private Text txt_LevelIndex;
    [SerializeField] private Image img_Done;

    public void SetButtonPosition(Vector2 _pos)
    {
        this.GetComponent<RectTransform>().anchoredPosition = _pos;
    }

    public void SetButtonLevelTitle(string _levelTitle)
    {
        txt_LevelIndex.text = _levelTitle;
    }

    public void SetLevelIsDone(bool _isDone)
    {
        img_Done.enabled = _isDone;
    }
}
