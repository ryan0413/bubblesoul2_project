﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_MainMenu : MonoBehaviour
{
    [SerializeField] private WorldSelection worldSelection;
    [SerializeField] private Setting setting;

    public void InitMenuSetting()
    {
        worldSelection.SetCanvas_MainMenuRef(this);
        setting.SetCanvas_MainMenuRef(this);
        worldSelection.gameObject.SetActive(true);
        setting.gameObject.SetActive(true);
        setting.OnOpenSettingPanel(false);
    }

    private void Start()
    {
        GameManager.Instance.m_GamePlayStat = GameManager.GamePlayStat.MainMenu;
        InitMenuSetting();
    }

    public void OnOpenSettingPanel()
    {
        setting.OnOpenSettingPanel(true);
        worldSelection.OnCloseCheckLevelPanel();
    }

    public void SettingEvent_DeleteAllLevelDatas()
    {
        GlobalSave.Instance.DeleteAllPrefs();
        worldSelection.ResetAllLevelDone();
    }
}
