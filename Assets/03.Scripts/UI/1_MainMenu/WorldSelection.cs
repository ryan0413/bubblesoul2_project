﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WorldSelection : MonoBehaviour
{
    [SerializeField] private GameObject m_WorldMapGroup;
    [SerializeField] private GameObject m_DotGroup;
    [SerializeField] private Text m_WorldTitle;
    [SerializeField] private GameObject m_CheckLevelGroup;

    private int totalWorldCount;
    private int currentWorldIndex;
    public int CurrentWorldIndex() { return currentWorldIndex; }
    private int currentLevelIndex;

    private List<WorldMap> worldMaps;
    private LevelDatas selectionLevelDatas;
    private List<LevelInformation> levelInformationList;

    private Canvas_MainMenu m_Canvas_MainMenu;
    public void SetCanvas_MainMenuRef(Canvas_MainMenu _canvas_MainMenu) { m_Canvas_MainMenu = _canvas_MainMenu; }

    private int GetTotalWorldCount()
    {
        worldMaps = new List<WorldMap>();
        levelInformationList = new List<LevelInformation>();
        int count = 0;
        if (m_WorldMapGroup != null)
        {
            for (int i = 0; i < m_WorldMapGroup.transform.childCount; i++)
            {
                if (m_WorldMapGroup.transform.GetChild(i).GetComponent<WorldMap>() != null)
                {
                    WorldMap worldMap = m_WorldMapGroup.transform.GetChild(i).GetComponent<WorldMap>();
                    worldMaps.Add(worldMap);
                    worldMap.InitWorldDatas(LevelManager.Instance.worldDatas[i], (callback) => 
                    {
                        foreach (LevelInformation item in callback)
                            levelInformationList.Add(item);
                    });
                    worldMap.SetWorldSelectionRef(this);
                    worldMap.GetComponent<CanvasGroup>().alpha = 0f;
                    count += 1;                    
                }
                else
                {
                    continue;
                }
            }
        }
        Debug.Log("World Count = " + count);
        return count;
    }

    private void Start()
    {
        m_WorldMapGroup.SetActive(true);
        m_DotGroup.SetActive(true);
        m_CheckLevelGroup.SetActive(false);
        totalWorldCount = GetTotalWorldCount();
        currentWorldIndex = 1;
        JumpWorldSelection(CurrentWorldIndex());
    }

    private void JumpWorldSelection(int _worldIndex)
    {
        foreach (WorldMap worldMap in worldMaps)
        {
            worldMap.ShowWorldMap(_worldIndex, (callbackWorldTitle) =>
            {
                if (callbackWorldTitle != "")
                {
                    m_WorldTitle.text = callbackWorldTitle;
                }
            });
        }
        SetDot(_worldIndex);
    }

    private void SetDot(int _worldIndex)
    {
        for (int i = 0; i < m_DotGroup.transform.childCount; i++)
        {
            if (i + 1 == _worldIndex)
            {
                m_DotGroup.transform.GetChild(i).Find("Selection").gameObject.SetActive(true);
            }
            else
            {
                m_DotGroup.transform.GetChild(i).Find("Selection").gameObject.SetActive(false);
            }
        }
    }

    public void ResetAllLevelDone()
    {
        if (levelInformationList.Count > 0)
        {
            foreach (LevelInformation item in levelInformationList)
            {
                item.SetLevelIsDone(false);
            }
        }
    }

    #region Button Event
    public void OnChangeWorldNext()
    {
        if (CurrentWorldIndex() < totalWorldCount)
        {
            currentWorldIndex += 1;
            JumpWorldSelection(CurrentWorldIndex());
        }
    }

    public void OnChangeWorldPrevious()
    {
        if (CurrentWorldIndex() > 1)
        {
            currentWorldIndex -= 1;
            JumpWorldSelection(CurrentWorldIndex());
        }
    }
    
    public void OnJumpWorld(int _worldIndex)
    {
        currentWorldIndex = _worldIndex;
        JumpWorldSelection(CurrentWorldIndex());
    }

    /// <summary>
    /// 開啟關卡資訊面板
    /// </summary>
    public void OnOpenCheckLevelPanel(LevelDatas _levelDatas)
    {
        LevelSave _levelSave = GlobalSave.Instance.GetCurrentSelectionLevelSave(_levelDatas.levelTitle);
        if (_levelSave != null) 
        {
            //Debug.Log(_levelSave.levelTitle + "/" + _levelSave.totalScore);
            if (GlobalSave.Instance.CheckPreviewLevelIsDone(_levelSave) == true)
            {
                m_CheckLevelGroup.SetActive(true);
                m_CheckLevelGroup.transform.Find("txt_LevelTitle").GetComponent<Text>().text = _levelDatas.levelTitle;
                m_CheckLevelGroup.transform.Find("txt_BestScore").GetComponent<Text>().text = "BEST SCORE : " + _levelSave.totalScore;
                selectionLevelDatas = _levelDatas;
            }
            else
            {
                Debug.Log("未完成前一關卡");
                // Dialogue Manager
                DialogueManager.Instance.ShowDialogue("未完成前一關卡");
                return;
            }            
        }
        else
        {
            Debug.Log("LevelSave Data is empty...");
        }
    }

    public void OnCloseCheckLevelPanel()
    {
        m_CheckLevelGroup.SetActive(false);
        selectionLevelDatas = null;
    }

    public void OnPlayLevel()
    {
        if (selectionLevelDatas != null)
        {
            Debug.Log("Go! " + selectionLevelDatas.levelTitle);
            GameManager.Instance.AssignCurrentLevelDatas(selectionLevelDatas);
            SceneManager.LoadScene("99_AsyncLoading");
        }
    }
    #endregion
}
