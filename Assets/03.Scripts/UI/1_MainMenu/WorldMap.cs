﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class WorldMap : MonoBehaviour
{
    private WorldDatas worldDatas;
    public int WorldIndex() { return worldDatas.worldIndex; }
    private WorldSelection worldSelectionRef;
    public void SetWorldSelectionRef(WorldSelection _worldSelection) { worldSelectionRef = _worldSelection; }

    public void InitWorldDatas(WorldDatas _worldDatas, Action<List<LevelInformation>> _levelInformationListCallBack)
    {
        //Debug.Log("InitWorldDatas");
        worldDatas = _worldDatas;
        List<LevelInformation> levelInformationListCallBack = new List<LevelInformation>();
        for (int i = 0; i < worldDatas.levelDatas.Length; i++)
        {
            // Button
            LevelDatas levelDatas = worldDatas.levelDatas[i];

            LevelInformation levelBtn = Instantiate(levelDatas.m_LevelButton, this.transform).GetComponent<LevelInformation>();
            levelBtn.SetButtonPosition(levelDatas.m_ButtonPosition);
            levelBtn.SetButtonLevelTitle(string.Format("{0}-{1}", worldDatas.worldIndex, worldDatas.levelDatas[i].levelIndex));

            levelInformationListCallBack.Add(levelBtn);
            
            GlobalSave.Instance.SetLevelSave(levelDatas, (callBack) => { levelBtn.SetLevelIsDone(callBack); });

            // Listen
            EventTrigger trigger = levelBtn.GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerClick;
            entry.callback.AddListener((eventData) => { worldSelectionRef.OnOpenCheckLevelPanel(levelDatas); });
            trigger.triggers.Add(entry);
        }
        _levelInformationListCallBack(levelInformationListCallBack);
    }
    
    public void ShowWorldMap(int _index, Action<string> callbackWorldTitle)
    {
        if (_index == worldDatas.worldIndex)
        {
            GetComponent<CanvasGroup>().alpha = 1f;
            callbackWorldTitle(worldDatas.worldTitle);
        }
        else
        {
            GetComponent<CanvasGroup>().alpha = 0f;
            callbackWorldTitle("");
        }
    }
}