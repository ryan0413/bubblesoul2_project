﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : SingletonManager<LevelManager>
{
    public List<WorldDatas> worldDatas = new List<WorldDatas>();
    [HideInInspector] public List<LevelDatas> levelDatas = new List<LevelDatas>();
    [HideInInspector] public LevelDatas nextLevelDatas;

    public int currentWorldIndex;
    public int currentLevelIndex;

    private void Start()
    {
        for (int i = 0; i < worldDatas.Count; i++)
        {
            for (int j = 0; j < worldDatas[i].levelDatas.Length; j++)
            {
                levelDatas.Add(worldDatas[i].levelDatas[j]);
            }
        }
    }

    public void GetNextLevelDatas(LevelDatas _levelDatas)
    {
        for (int i = 0; i < levelDatas.Count; i++)
        {
            if (_levelDatas.levelTitle == levelDatas[i].levelTitle)
            {
                if (i == levelDatas.Count - 1) // end
                {
                    nextLevelDatas = null;
                }
                else
                {
                    nextLevelDatas = levelDatas[i + 1];
                }
            }
            else
            {
                continue;
            }
        }
    }
}
