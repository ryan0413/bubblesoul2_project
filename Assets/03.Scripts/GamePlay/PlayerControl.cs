﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Com.LuisPedroFonseca.ProCamera2D;

public class PlayerControl : MonoBehaviour
{
    public PlayerData m_PlayerData;
    public int interactiveObjectsCount = 0;//身上擁有道具數
    public bool isInBuoyancyArea = false;//是否在水裡
    public bool isInAttractionArea = false;//是否受到引力物件影響
    public Camera mainCamera;
    public Vector3 m_mousePosition;
    public GameObject m_Model;
    [Header("Generate GameObject")]
    public GameObject eatCoinPrefab;//吃金幣效果
    public GameObject moveTrail;//拖曳特效
    [Space(10)]
    private Rigidbody2D m_Rigidbody;
    [Header("Player")]
    private int m_PlayerHealth = 3;
    private int m_Stamina = 100;
    private float m_MoveSpeed = 20;
    private float m_GravityScale = 1;

    private float startDashSpeed = 200.0f;//起始加速值
    
    public GameObject m_StartPower;

    [HideInInspector]
    public bool isReadyTime = true;
    public Transform m_StartPoint;

    private bool isFirstTap = true;
    private bool isItemEffect = false;

    private float m_LimitSpeed = 5;   

    private int width, height;

    private void Awake()
    {
        m_Rigidbody = this.GetComponent<Rigidbody2D>();
    }

    private void Start()
    {     
        m_PlayerHealth = m_PlayerData.m_PlayerHealth;
        m_Stamina = m_PlayerData.m_Stamina;
        m_MoveSpeed = m_PlayerData.m_MoveSpeed;
        m_Rigidbody.mass = m_PlayerData.m_Mass;
        m_GravityScale = m_PlayerData.m_GravityScale;

        width = mainCamera.pixelWidth;
        height = mainCamera.pixelHeight;
    }

    /// <summary>
    /// 初始衝刺開始
    /// </summary>
    /// <returns></returns>
    public IEnumerator StartPowerUp()
    {
        GameManager.Instance.isPowerUp = false;
        m_Rigidbody.gravityScale = m_GravityScale;
        yield return new WaitForSeconds(1f);
        //GameManager.Instance.m_GamePlayStat = GameManager.GamePlayStat.Play;
    }

    private void Update()
    {        
        m_mousePosition = new Vector3(Mathf.Clamp((Input.mousePosition.x - width / 2) / (width / 2), -1, 1), Mathf.Clamp((Input.mousePosition.y - height / 2) / (height / 2), -1, 1), 0);

        //if (isReadyTime == true)
        //{
        //    if (isFirstTap == true)
        //    {
        //        this.transform.localPosition = m_StartPoint.localPosition;
        //        //m_Rigidbody.gravityScale = 0.0f;
        //        m_Rigidbody.IsSleeping();

        //        if (Input.GetMouseButton(0))
        //        {
        //            GameManager.Instance.isPowerUp = true;
        //            m_StartPower.SetActive(true);
        //        }
        //        else
        //        {
        //            m_StartPower.SetActive(false);
        //            if (GameManager.Instance.m_StartPower.timer >= 0.5f)
        //            {
        //                m_Rigidbody.AddForce(new Vector2(0, startDashSpeed), ForceMode2D.Impulse);
        //                isFirstTap = false;
        //                GameManager.Instance.m_StartPower.m_Text.text = "START !!!";
        //                StartCoroutine(StartPowerUp());
        //            }
        //            else
        //            {
        //                GameManager.Instance.isPowerUp = false;
        //            }
        //        }
        //    }
        //    return;
        //}
        //else
        //{
        //    m_Rigidbody.gravityScale = m_GravityScale;
        //}

        //持續按壓
        if (Input.GetMouseButton(0) && interactiveObjectsCount == 0) 
        {
            Vector3 screenPos = Input.mousePosition;
            float camZ_Pos = mainCamera.transform.localPosition.z;
            screenPos.z = 12.5f;
            //var newMousePosition = mainCamera.ScreenToWorldPoint(screenPos);
            //var forceDir = (newMousePosition - m_Rigidbody.transform.position).normalized;
            //var fixDir = new Vector3(forceDir.x / 2, forceDir.y, 0);

            var center_x = Screen.width / 2;
            var center_y = Screen.height / 2;
            var dis_x = screenPos.x - center_x;
            var dis_y = screenPos.y - center_y;
            //float rad1 = Mathf.Atan2(dis_y, dis_x);
            var rad = Mathf.Atan2(dis_y, dis_x);
            var v_x = -Mathf.Cos(rad);
            var v_y = -Mathf.Sin(rad);
            var moveDir = new Vector2(v_x, v_y);

            //fixDir = Vector3.ClampMagnitude(fixDir, m_LimitSpeed);
            //fixDir = transform.TransformDirection(fixDir);

            m_Rigidbody.AddForce(moveDir * m_MoveSpeed, ForceMode2D.Force); // Keep Move
            Debug.Log(moveDir);
        }

        //Max Speed
        if (isInBuoyancyArea == false || isInAttractionArea == false)
        {
            if (m_Rigidbody.velocity.y > m_LimitSpeed && interactiveObjectsCount == 0)
            {
                float y = m_Rigidbody.velocity.y;
                y--;
                m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, y, 0);
            }
            else if (m_Rigidbody.velocity.y < -m_LimitSpeed && interactiveObjectsCount == 0)
            {
                float y = m_Rigidbody.velocity.y;
                y++;
                m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, y, 0);
            }
            if (m_Rigidbody.velocity.x > m_LimitSpeed && interactiveObjectsCount == 0)
            {
                float x = m_Rigidbody.velocity.x;
                x--;
                m_Rigidbody.velocity = new Vector3(x, m_Rigidbody.velocity.y, 0);
            }
            else if (m_Rigidbody.velocity.x < -m_LimitSpeed && interactiveObjectsCount == 0)
            {
                float x = m_Rigidbody.velocity.x;
                x++;
                m_Rigidbody.velocity = new Vector3(x, m_Rigidbody.velocity.y, 0);
            }
        }
    }

    #region 碰撞偵測
    //碰撞偵測
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.CompareTag("Item"))
        {
            if (other.gameObject.GetComponent<Coin>() != null)
            {
                CheckPlayerCoinScore(other.gameObject.GetComponent<Coin>(), other.gameObject, eatCoinPrefab, false);
            }

            if (other.gameObject.GetComponent<Hurt>() != null)
            {
                CheckPlayerHealth(other.gameObject.GetComponent<Hurt>());
            }

            switch (other.transform.name)
            {
                case "SpeedItem"://衝氣波
                    //StartCoroutine(other.gameObject.GetComponent<Speed>().SpeedUp(m_Rigidbody));
                    break;
                case "AirFlowItem"://氣流通道
                    //StartCoroutine(other.gameObject.GetComponent<AirFlow>().AirPathFollow(m_Rigidbody));
                    break;
                case "PortalItem"://傳送門
                    //StartCoroutine(other.gameObject.GetComponent<Portal>().StartPortal(m_Rigidbody));
                    break;
                case "CloseDoorItem"://單次關閉門
                    //StartCoroutine(other.gameObject.GetComponent<CloseDoor>().StartCloseDoor(m_Rigidbody));
                    break;

                case "Grass"://草堆
                    other.gameObject.SetActive(false);
                    break;

                case "Finish"://終點
                    SceneManager.LoadScene("TestControl");
                    break;
            }            
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.transform.CompareTag("Item"))
        {
            switch (other.transform.name)
            {
                case "BuoyancyItem"://水域
                    isInBuoyancyArea = true;
                    break;
                case "AttractionItem"://吸引力
                    isInAttractionArea = true;
                    break;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.CompareTag("Item"))
        {
            switch (other.transform.name)
            {
                case "BuoyancyItem"://水域
                    isInBuoyancyArea = false;
                    break;
                case "AttractionItem"://吸引力
                    isInAttractionArea = false;
                    break;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Item"))
        {
            if (collision.gameObject.GetComponent<Coin>() != null)
            {
                CheckPlayerCoinScore(collision.gameObject.GetComponent<Coin>(), collision.gameObject, eatCoinPrefab, true);
            }

            switch (collision.transform.name)
            {
                case "Wood"://木堆
                    StartCoroutine(collision.gameObject.GetComponent<Wood>().ReboundOnce(m_Rigidbody));
                    break;
                case "Stone"://石堆
                    StartCoroutine(collision.gameObject.GetComponent<Stone>().ReboundOnce(m_Rigidbody));
                    break;
            }

            if (collision.gameObject.GetComponent<Hurt>() != null)
            {
                CheckPlayerHealth(collision.gameObject.GetComponent<Hurt>());
            }
        }
    }
    #endregion     

    /// <summary>
    /// 獲取分數
    /// </summary>
    /// <param name="coinClass">目標內容</param>
    /// <param name="target">目標物件</param>
    /// <param name="eatEffect">獲取特效</param>
    /// <param name="enable">是否單次獲取</param>
    private void CheckPlayerCoinScore(Coin coinClass, GameObject target, GameObject eatEffect, bool enable)
    {
        if (eatEffect != null)
        {
            GameObject obj = Instantiate(eatCoinPrefab);
            obj.transform.position = target.transform.position;
        }        
        //GameManager.Instance.GetCoin(coinClass.coinScore);
        target.SetActive(enable);
        //coinClass.enabled = enable;
    }

    private void CheckPlayerHealth(Hurt hurtClass)
    {
        m_Stamina -= hurtClass.hurt;
        if (m_Stamina <= 0)
        {
            m_PlayerHealth--;
            m_Stamina = m_PlayerData.m_Stamina;
            if (m_PlayerHealth <= 0)
            {
                SceneManager.LoadScene("TestControl");
            }
        }
    }
}