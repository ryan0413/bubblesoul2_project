﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class LevelDatas : ScriptableObject
{
    public int levelIndex;
    public string levelTitle;
    public string levelDescription;
    public GameObject m_LevelButton;
    public Vector2 m_ButtonPosition;
    public GameObject m_LevelContent;
}
