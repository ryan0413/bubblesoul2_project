﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class WorldDatas : ScriptableObject
{
    public int worldIndex;
    public string worldTitle;
    public string worldDescription;
    public LevelDatas[] levelDatas;	
}
