﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.Linq;

public class GlobalSave : SingletonManager<GlobalSave>
{
    public List<LevelSave> levelSave;

    private void Awake()
    {

    }

    private void Start()
    {
        Init();
    }

    public void Init()
    {
        levelSave = new List<LevelSave>();
    }

    public void SetLevelSave(LevelDatas _levelDatas, Action<bool> _isDoneCallBack)
    {
        if (levelSave.Count == LevelManager.Instance.levelDatas.Count)
        {
            Debug.Log("SetLevelSave");
            foreach (var item in levelSave)
            {
                if (item.levelTitle == _levelDatas.levelTitle)
                    _isDoneCallBack(item.isDone);
                else
                    continue;
            }
            return;
        }
        else
        {
            //Debug.Log("levelSave.Count error " + levelSave.Count);
        }

        LevelSave data = LoadLevelDataPrefs(_levelDatas.levelIndex, _levelDatas.levelTitle);
        levelSave.Add(data);
        _isDoneCallBack(data.isDone);
    }

    public void SaveLevelDatas(int _levelIndex, string _levelTitle, bool _isDone, int _time, int _score, int _totalScore)
    {
        Debug.Log("[SaveLevelDatas] : " + _levelTitle);
        if (levelSave.Count > 0)
        {
            for (int i = 0; i < levelSave.Count; i++)
            {
                if (_levelTitle == levelSave[i].levelTitle)
                {
                    levelSave[i].levelIndex = _levelIndex;
                    levelSave[i].levelTitle = _levelTitle;
                    levelSave[i].isDone = _isDone;
                    levelSave[i].time = _time;
                    levelSave[i].score = _score;
                    levelSave[i].totalScore = _totalScore;

                    SaveLevelDataPrefs(levelSave[i]);
                }
                else
                {
                    continue;
                }
            }
        }
    }

    public void SaveLevelDataPrefs(LevelSave _levelSave)
    {
        StringBuilder saver = new StringBuilder();
        saver.AppendFormat("levelindex={0}|", _levelSave.levelIndex);
        saver.AppendFormat("levelTitle={0}|", _levelSave.levelTitle);
        saver.AppendFormat("isdone={0}|", _levelSave.isDone == true ? "true" : "false");
        saver.AppendFormat("time={0}|", _levelSave.time);
        saver.AppendFormat("score={0}|", _levelSave.score);
        saver.AppendFormat("totalscore={0}", _levelSave.totalScore);

        PlayerPrefs.SetString(_levelSave.levelTitle, saver.ToString());

        Debug.Log("儲存資料 : " + PlayerPrefs.GetString(_levelSave.levelTitle));
    }

    public LevelSave LoadLevelDataPrefs(int _levelIndex, string _levelTitle)
    {
        //Debug.Log("[LoadLevelDataPrefs] : " + _levelTitle);
        if (PlayerPrefs.HasKey(_levelTitle))
        {
            LevelSave loader = new LevelSave();
            string datas = PlayerPrefs.GetString(_levelTitle);

            Debug.Log(datas);

            string[] dataArray = datas.Split('|');
            foreach (string data in dataArray)
            {
                string[] typeArray = data.Split('=');
                switch (typeArray[0])
                {
                    case "levelindex":
                        loader.levelIndex = int.Parse(typeArray[1]);
                        break;
                    case "levelTitle":
                        loader.levelTitle = typeArray[1];
                        break;
                    case "isdone":
                        loader.isDone = typeArray[1] == "true" ? true : false;
                        break;
                    case "time":
                        loader.time = int.Parse(typeArray[1]);
                        break;
                    case "score":
                        loader.score = int.Parse(typeArray[1]);
                        break;
                    case "totalscore":
                        loader.totalScore = int.Parse(typeArray[1]);
                        break;
                    default:
                        break;
                }
            }
            return loader;
        }
        else
        {
            return new LevelSave()
            {
                levelIndex = _levelIndex,
                levelTitle = _levelTitle,
                isDone = false,
                time = 0,
                score = 0,
                totalScore = 0
            };
        }
    }

    public LevelSave GetCurrentSelectionLevelSave(string _levelTitle)
    {
        Debug.Log("Now select level : " + _levelTitle);

        if (levelSave == null || levelSave.Count <= 0)
        {
            Debug.Log("levelSave == null or levelSave.Count <= 0");
            return null;
        }

        foreach (var item in levelSave)
        {
            if (_levelTitle == item.levelTitle)
            {
                //Debug.Log("levelSave levelTitle : " + item.levelTitle);
                return item;
            }
            else
            {
                continue;
            }                
        }
            
        return null;
    }

    public void DeleteAllPrefs()
    {
        PlayerPrefs.DeleteAll();
        if (levelSave.Count > 0)
        {
            Debug.Log("Delete All Level, Count = " + levelSave.Count);
            foreach (LevelSave item in levelSave)
            {
                item.levelIndex = 0;
                item.isDone = false;
                item.time = 0;
                item.score = 0;
                item.totalScore = 0;
            }
        } 
    }

    // Check Preview Level is done
    public bool CheckPreviewLevelIsDone(LevelSave _currentLevel)
    {
        if (_currentLevel.isDone == true)
            return true;

        for (int i = 0; i < levelSave.Count; i++)
        {
            if (_currentLevel.levelTitle == levelSave[i].levelTitle)
            {
                if (i == 0)
                {
                    return true;
                }
                else
                {
                    if (levelSave[i - 1].isDone == true)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }            
        }

        return false;
    }
}

[Serializable]
public class LevelSave
{
    public int levelIndex;
    public string levelTitle;
    public bool isDone;
    public int time;
    public int score;
    public int totalScore;
}