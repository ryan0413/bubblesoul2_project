﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayerData : ScriptableObject
{
    public int m_PlayerHealth = 3;
    public int m_Stamina = 100;
    public float m_MoveSpeed = 20;
    public float m_Mass = 1;
    public float m_GravityScale = 1;
}
