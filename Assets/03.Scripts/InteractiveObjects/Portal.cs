﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 傳送門
/// </summary>
public class Portal : MonoBehaviour
{
    [Header("Point")]
    [SerializeField] private GameObject startPoint;
    [SerializeField] private GameObject startDirPoint;
    [SerializeField] private GameObject endPoint;
    [SerializeField] private GameObject endDirPoint;
    [Space(10)]
    [SerializeField] private float catchWaitTime = 0.02f;
    [SerializeField] private float forceTime = 0.25f;
    [SerializeField] private float forcePower = 600;

    public bool oncePlay = false;    
    
    public IEnumerator StartPortal(Player m_Player)
    {
        m_Player.SetPlayerFreeze(true);

        Vector2 direction = endDirPoint.transform.position - endPoint.transform.position;
        Vector2 localDirection = transform.InverseTransformDirection(direction) * -1;
        Debug.Log(localDirection);

        m_Player.transform.position = startPoint.transform.position;

        yield return new WaitForSeconds(catchWaitTime);

        m_Player.transform.position = endPoint.transform.position;        

        Vector2 startPos = endPoint.transform.position;
        Vector2 endPos = startPos + (localDirection * 2);

        m_Player.transform.position = startPos;

        yield return new WaitUntil(() =>
        {
            float distance = Vector3.Distance(m_Player.transform.position, endPos);
            m_Player.transform.position = Vector3.Lerp(m_Player.transform.position, endPos, 2.5f * Time.deltaTime);
            return distance <= (Vector3.Distance(startPos, endPos)) / 20f;
        });

        m_Player.SetPlayerFreeze(false);

        yield return new WaitForSeconds(forceTime);        

        if (oncePlay == true)
        {
            this.gameObject.SetActive(false);
        }
    }
}
