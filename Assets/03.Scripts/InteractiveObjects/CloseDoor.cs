﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CloseDoor : MonoBehaviour
{
    [Range((int)2, (int)16)] public int count = 5;
    public Transform closeMotion;

    public Transform lineTask;

    public enum CloseForceDir { Up, Down, Left, Right }
    public CloseForceDir closeForceDir = CloseForceDir.Up;

    public Transform marking;

    private void Start()
    {
        marking.gameObject.SetActive(true);
        foreach (Transform item in closeMotion)
        {
            item.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        if (!Application.isPlaying)
            SetLengh();
    }

    /// <summary>
    /// 設置長度
    /// </summary>
    private void SetLengh()
    {
        if (closeMotion == null)
            return;

        for (int i = 0; i < closeMotion.childCount; i++)
        {
            if (i < count)
            {
                closeMotion.GetChild(i).gameObject.SetActive(true);
            }
            else
            {
                closeMotion.GetChild(i).gameObject.SetActive(false);
            }
        }

        lineTask.localScale = new Vector3((float)count / 2, 1, 1);
        this.GetComponent<BoxCollider2D>().offset = new Vector2((float)count / 4, 0);
        this.GetComponent<BoxCollider2D>().size = new Vector2((float)count / 2, 0.05f);
    }    

    public IEnumerator StartCloseDoor(Player m_Player)
    {
        //switch (closeForceDir)
        //{
        //    case CloseForceDir.Up:
        //        Vector3 endPosUp = m_Transform.position + Vector3.up * 2;
        //        yield return new WaitUntil(() =>
        //        {
        //            float distance = Vector3.Distance(m_Transform.position, endPosUp);
        //            m_Transform.position = Vector3.Lerp(m_Transform.position, endPosUp, 2.5f * Time.deltaTime);
        //            return distance <= (Vector3.Distance(m_Transform.position, endPosUp)) / 20f;
        //        });
        //        break;
        //    case CloseForceDir.Down:
        //        Vector3 endPosDown = m_Transform.position + Vector3.down * 2;
        //        yield return new WaitUntil(() =>
        //        {
        //            float distance = Vector3.Distance(m_Transform.position, endPosDown);
        //            m_Transform.position = Vector3.Lerp(m_Transform.position, endPosDown, 2.5f * Time.deltaTime);
        //            return distance <= (Vector3.Distance(m_Transform.position, endPosDown)) / 20f;
        //        });
        //        break;
        //    case CloseForceDir.Right:
        //        Vector3 endPosRight = m_Transform.position + Vector3.right * 2;
        //        yield return new WaitUntil(() =>
        //        {
        //            float distance = Vector3.Distance(m_Transform.position, endPosRight);
        //            m_Transform.position = Vector3.Lerp(m_Transform.position, endPosRight, 2.5f * Time.deltaTime);
        //            return distance <= (Vector3.Distance(m_Transform.position, endPosRight)) / 20f;
        //        });
        //        break;
        //    case CloseForceDir.Left:
        //        Vector3 endPosLeft = m_Transform.position + Vector3.left * 2;
        //        yield return new WaitUntil(() =>
        //        {
        //            float distance = Vector3.Distance(m_Transform.position, endPosLeft);
        //            m_Transform.position = Vector3.Lerp(m_Transform.position, endPosLeft, 2.5f * Time.deltaTime);
        //            return distance <= (Vector3.Distance(m_Transform.position, endPosLeft)) / 20f;
        //        });
        //        break;
        //}

        yield return new WaitForSeconds(0.15f);

        this.GetComponent<BoxCollider2D>().isTrigger = false;
        marking.gameObject.SetActive(false);
        foreach (Transform item in closeMotion)
        {
            item.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.0125f);
        }
        this.GetComponent<BoxCollider2D>().enabled = false;
    }
}
