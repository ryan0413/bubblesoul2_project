﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 氣流通道
/// </summary>
public class AirFlow : MonoBehaviour
{
    public EditorPathScript PathToFollow;

    private int CurrentWayPointID = 0;
    public float speed = 10;

    private float reachDistacne = 0.1f;    

    public IEnumerator AirPathFollow(Player m_Player)
    {
        CurrentWayPointID = 0;

        if (PathToFollow == null)
        {
            yield return null;
        }

        while (CurrentWayPointID < PathToFollow.path_objs.Count)
        {
            float distance = Vector2.Distance(PathToFollow.path_objs[CurrentWayPointID].position, m_Player.transform.position);
            m_Player.transform.position = Vector2.MoveTowards(m_Player.transform.position, PathToFollow.path_objs[CurrentWayPointID].position, Time.fixedDeltaTime * speed);
            if (distance <= reachDistacne)
            {
                CurrentWayPointID++;
            }
            yield return null;
        }
        Vector2 forceDir = (PathToFollow.path_objs[PathToFollow.path_objs.Count - 1].position - PathToFollow.path_objs[PathToFollow.path_objs.Count - 2].position).normalized;
    }
}
