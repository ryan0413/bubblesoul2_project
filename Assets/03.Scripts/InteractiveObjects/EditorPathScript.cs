﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 線段
/// </summary>
[ExecuteInEditMode]
public class EditorPathScript : MonoBehaviour
{
    public Color rayColor = Color.white;
    public List<Transform> path_objs = new List<Transform>();
    private Transform[] theArray;

    private LineRenderer lineRenderer;
    private int path_objsCount;

    private void OnDrawGizmos()
    {
        Gizmos.color = rayColor;
        theArray = this.GetComponentsInChildren<Transform>();
        path_objs.Clear();

        if (theArray.Length != path_objs.Count)
        {
            int id = 0;
            foreach (Transform path_obj in theArray)
            {
                if (path_obj != this.transform)
                {
                    path_obj.name = id.ToString();
                    path_objs.Add(path_obj);
                    id += 1;
                }
            }
        }        

        for (int i = 0; i < path_objs.Count; i++)
        {
            Vector3 position = path_objs[i].position;
            if (i > 0)
            {
                Vector3 previous = path_objs[i - 1].position;
                Gizmos.DrawLine(previous, position);
                Gizmos.DrawWireSphere(position, 0.175f);
            }
        }
    }

    private void Update()
    {
        if (lineRenderer == null)
        {
            lineRenderer = this.GetComponent<LineRenderer>();
        }

        lineRenderer.positionCount = path_objs.Count;

        for (int i = 0; i < lineRenderer.positionCount; i++)
        {
            lineRenderer.SetPosition(i, path_objs[i].localPosition);
        }

        path_objsCount = path_objs.Count;
    }
}
