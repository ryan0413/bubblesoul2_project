﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wood : MonoBehaviour
{
    public int reboundCount = 0;

    public IEnumerator ReboundOnce(Rigidbody2D m_Rigidbody)
    {
        reboundCount--;
        if (reboundCount < 0)
        {
            this.gameObject.SetActive(false);
        }
        yield return null;
    }
}
