﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Speed : MonoBehaviour
{   
    public float dashLength = 20;

    private Color rayColor = Color.white;

    private void OnDrawGizmos()
    {
        Gizmos.color = rayColor;
        Gizmos.DrawLine(this.transform.position, this.transform.position + (Vector3.up * dashLength));
        Gizmos.DrawWireSphere(this.transform.position + (Vector3.up * dashLength), 0.3f);
    }

    public IEnumerator SpeedUp(Player m_Player)
    {
        m_Player.SetPlayerFreeze(true);

        Vector3 startPos = this.transform.position;
        Vector3 endPos = startPos + (Vector3.up * dashLength);

        m_Player.transform.position = startPos;

        yield return new WaitUntil(() =>
        {
            float distance = Vector3.Distance(m_Player.transform.position, endPos);
            m_Player.transform.position = Vector3.Lerp(m_Player.transform.position, endPos, 2.5f * Time.deltaTime);
            return distance <= (Vector3.Distance(startPos, endPos)) / 20f;
        });

        m_Player.SetPlayerFreeze(false);
    }
}
