using System;
using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(SimpleRotate))]
public class CrossWall : MonoBehaviour
{
    [Range((int)0,(int)4)]
    public int upCount = 4, leftCount = 4, downCount = 4, rightCount = 4;
    private Transform upGroup, leftGroup, downGroup, rightGroup;

    private void Update () 
	{
        if (!Application.isPlaying)
            SetLengh();
    }

    /// <summary>
    /// �]�m����
    /// </summary>
    private void SetLengh()
    {
        if (upGroup == null)
        {
            upGroup = this.transform.Find("Up");
        }
        for (int i = 0; i < upGroup.childCount; i++)
        {
            if (i < upCount)
            {
                upGroup.GetChild(i).gameObject.SetActive(true);
            }
            else
            {
                upGroup.GetChild(i).gameObject.SetActive(false);
            }
        }

        if (leftGroup == null)
        {
            leftGroup = this.transform.Find("Left");
        }
        for (int i = 0; i < leftGroup.childCount; i++)
        {
            if (i < leftCount)
            {
                leftGroup.GetChild(i).gameObject.SetActive(true);
            }
            else
            {
                leftGroup.GetChild(i).gameObject.SetActive(false);
            }
        }

        if (downGroup == null)
        {
            downGroup = this.transform.Find("Down");
        }
        for (int i = 0; i < downGroup.childCount; i++)
        {
            if (i < downCount)
            {
                downGroup.GetChild(i).gameObject.SetActive(true);
            }
            else
            {
                downGroup.GetChild(i).gameObject.SetActive(false);
            }
        }

        if (rightGroup == null)
        {
            rightGroup = this.transform.Find("Right");
        }
        for (int i = 0; i < rightGroup.childCount; i++)
        {
            if (i < rightCount)
            {
                rightGroup.GetChild(i).gameObject.SetActive(true);
            }
            else
            {
                rightGroup.GetChild(i).gameObject.SetActive(false);
            }
        }
    }
}
