﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
[RequireComponent(typeof(CircleCollider2D))]
public class Attraction : MonoBehaviour
{
    [SerializeField] private CircleCollider2D circleCollider2D;

    private void Update()
    {
        if (!Application.isPlaying)
            SetSize();
    }

    private void SetSize()
    {
        if (this.transform.Find("Model") == null)
            return;

        float size = circleCollider2D.radius * 2;
        this.transform.Find("Model").transform.localScale = new Vector3(size, size, size);
    }
}
