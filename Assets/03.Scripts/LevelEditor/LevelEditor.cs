﻿using UnityEngine;

public class LevelEditor : MonoBehaviour
{
    public Texture2D map;

    public ColorToPrefab[] colorMappings;

	void Start ()
    {
        if (map == null)
        {
            Debug.Log("No Map!!");
            return;
        }
        GenerateLevel();
	}

    void GenerateLevel()
    {
        for (int x = 0; x < map.width; x++)
        {
            for (int y = 0; y < map.height; y++)
            {
                GenerateTile(x, y);
            }
        }
    }

    void GenerateTile(int x, int y)
    {
        Color pixelColor = map.GetPixel(x, y);

        //空的區域
        if (pixelColor.a == 0)
        {
            return;
        }

        foreach (ColorToPrefab colorMapping in colorMappings)
        {
            if (colorMapping.color.Equals(pixelColor))
            {
                Vector2 position = new Vector2(x, y);
                GameObject obj = Instantiate(colorMapping.prefab, position, Quaternion.identity, transform);
                obj.name = colorMapping.name + obj.transform.position;
                if (colorMapping.parent != null)
                {
                    obj.transform.SetParent(colorMapping.parent.transform);
                }
            }
        }
    }
}
