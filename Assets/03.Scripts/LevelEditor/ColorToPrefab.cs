﻿using UnityEngine;

[System.Serializable]
public class ColorToPrefab
{
    public Color color;
    public GameObject prefab;
    public string name;
    public GameObject parent;
}
