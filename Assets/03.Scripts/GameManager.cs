﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// 控制整體遊戲流程
/// </summary>
public class GameManager : SingletonManager<GameManager>
{
    public enum GamePlayStat { InitLoading, MainMenu, AsyncLoading, ReadyToGo, Playing, Finish }
    public GamePlayStat m_GamePlayStat = GamePlayStat.InitLoading;

    [SerializeField] private Player m_PlayerPrefab;
    private Player player;
    private LevelDatas currentLevelDatas;
    public LevelDatas GetCurrentLevelsDatas() { return currentLevelDatas; }

    private UIManager uiManager;

    [HideInInspector] public bool isPowerUp;
    [HideInInspector] public bool isFirstTap;
    [HideInInspector] public bool isFinishTiming;

    private LevelContent currentLevelContent;

    private void Awake()
    {
        
    }

    private Player AssignPlayer()
    {
        Player currentPlayer = Instantiate(m_PlayerPrefab).GetComponent<Player>();
        currentPlayer.InitPlayerDatas();
        CameraController.Instance.AssignPlayerRef(currentPlayer);
        return currentPlayer;
    }

    private UIManager AssignUIManager()
    {
        return FindObjectOfType<UIManager>();
    }

    private void Start()
    {

    }

    private void Update()
    {
        switch (m_GamePlayStat)
        {
            case GamePlayStat.InitLoading:

                break;
            case GamePlayStat.MainMenu:

                break;
            case GamePlayStat.AsyncLoading:

                break;
            case GamePlayStat.ReadyToGo:
                ReadyToGo();
                break;
            case GamePlayStat.Playing:
                GamePlayingState();
                break;
            case GamePlayStat.Finish:
                FinishGame();
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// 初始衝刺
    /// </summary>
    private void ReadyToGo()
    {
        if (GetCurrentLevelDatas() == null)
        {
            Debug.Log("LevelDatas not found");
            return;
        }            

        if (player == null)
        {
            player = AssignPlayer(); // 建立 Player 參照      
            
            isFirstTap = true;
            isPowerUp = false;

            LoadLevelDatas(currentLevelDatas);
            if (currentLevelContent == null)
                return;
        }

        if (uiManager == null)
        {
            uiManager = AssignUIManager(); // 建立 UIManager 參照
            uiManager.InitializeGamePlayUI();
        }

        if (isFirstTap == true)
        {
            player.SetPlayerFreeze(true);

            player.transform.localPosition = currentLevelContent.GetForceStartPoint(); // Lock player to start point

            if (Input.GetMouseButton(0))
            {
                isPowerUp = true;
                player.SetStartDashPower(true);
            }
            else
            {
                player.SetStartDashPower(false);

                if (uiManager.GetStartPowerTimer() >= 0.5f)
                {
                    isFirstTap = false;
                    uiManager.StartDashUp();
                    player.StartPowerUp(currentLevelContent.GetForceStartPoint(), currentLevelContent.GetForceEndPoint());
                }
                else
                {
                    isPowerUp = false;
                }
            }
        }
    }

    private void GamePlayingState()
    {
        uiManager.HidePowerUpUI();
    }

    private void FinishGame()
    {
        if (isFinishTiming == false)
            return;

        if (uiManager != null)
        {
            player.SetPlayerFreeze(true);
            uiManager.OpenFinishPanel();

            uiManager.GetLevelFinishInformation((data) =>
            {
                if (data != null)
                {
                    GlobalSave.Instance.SaveLevelDatas(data.levelIndex, data.levelTitle, data.isDone, data.time, data.score, data.totalScore);
                }
            });
        }

        isFinishTiming = false;
        //m_GamePlayStat = GamePlayStat.MainMenu;
        //SceneManager.LoadScene("1_MainMenu");
    }

    /// <summary>
    /// 關卡初始狀態
    /// </summary>
    public void InitializeGame()
    {
        m_GamePlayStat = GamePlayStat.ReadyToGo;
        isFirstTap = true;            
    }

    public void AssignCurrentLevelDatas(LevelDatas _levelDatas)
    {
        currentLevelDatas = _levelDatas;
    }

    public LevelDatas GetCurrentLevelDatas()
    {
        if(currentLevelDatas != null)
        {
            return currentLevelDatas;
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// 載入關卡
    /// </summary>
    public void LoadLevelDatas(LevelDatas _levelDatas)
    {
        currentLevelContent = Instantiate(currentLevelDatas.m_LevelContent).GetComponent<LevelContent>();
        currentLevelContent.InitLevel(currentLevelDatas);
    }

    public void AddCoin(int _score)
    {
        uiManager.AddCoin(_score);
    }

    public void OnPlayLevelAgain()
    {
        SceneManager.LoadScene("99_AsyncLoading");
    }

    public void OnPlayNextLevel()
    {
        LevelManager.Instance.GetNextLevelDatas(currentLevelDatas);
        if(LevelManager.Instance.nextLevelDatas != null)
        {
            AssignCurrentLevelDatas(LevelManager.Instance.nextLevelDatas);
            SceneManager.LoadScene("99_AsyncLoading");
        }
        else
        {
            Debug.Log("已完成所有關卡!");
            SceneManager.LoadScene("1_MainMenu");
        }
    }
}