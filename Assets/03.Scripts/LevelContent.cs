﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelContent : MonoBehaviour
{
    private Vector3 m_ForceStartPoint;
    private Vector3 m_ForceEndPoint;

    public void InitLevel(LevelDatas _levelDatas)
    {
        transform.position = Vector3.zero;
        m_ForceStartPoint = transform.Find("ForceStartPoint").transform.position;
        m_ForceEndPoint = transform.Find("ForceEndPoint").transform.position;

        Debug.Log("載入場景:" + _levelDatas.levelTitle);
    }

    public Vector3 GetForceStartPoint()
    {
        return m_ForceStartPoint;
    }

    public Vector3 GetForceEndPoint()
    {
        return m_ForceEndPoint;
    }
}
