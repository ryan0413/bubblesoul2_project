﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : SingletonManager<DialogueManager>
{
    [SerializeField] private Canvas m_Canvas;

    [SerializeField] private DialogueElement m_DialogueElement;
    [SerializeField] private RectTransform m_DialogueLayout;

    public List<DialogueElement> currentDialogueElementList;
    private int maxCount = 10;

    private void Init()
    {
        currentDialogueElementList = new List<DialogueElement>();
        for (int i = 0; i < maxCount; i++)
        {
            currentDialogueElementList.Add(CreateDialogueElement());
        }
    }

    private void Start()
    {
        Init();
    }

    public void ShowDialogue(string _content)
    {
        int useIndex = GetUseElement();   
        //ShowDialogue(_content, 10f);
    }

    //public void ShowDialogue(string _content, float _duration)
    //{
    //    DialogueElement instance = CreateDialogueElement();
    //    instance.SetContent(_content);
    //}

    private int GetUseElement()
    {
        int returnObjIndex = 0;
        for (int i = 0; i < currentDialogueElementList.Count; i++)
        {
            if (currentDialogueElementList[i].gameObject.activeInHierarchy == true)
            {
                continue;
            }
            else
            {
                returnObjIndex = i;
            }
        }
        return returnObjIndex;
    }

    private DialogueElement CreateDialogueElement()
    {
        DialogueElement instance = Instantiate(m_DialogueElement, m_DialogueLayout);
        instance.SetContent("");
        instance.gameObject.SetActive(false);
        //instance.CloseContent();
        return instance;
    }

    private void Update()
    {
        
    }
}
