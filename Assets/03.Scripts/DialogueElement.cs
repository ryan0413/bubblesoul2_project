﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueElement : MonoBehaviour
{
    [SerializeField] private Text m_Content;

    private void Start()
    {
        
    }

    private void Update()
    {
        
    }

    public void OpenContent()
    {

    }

    public void CloseContent()
    {

    }

    public void SetContent(string _content)
    {
        m_Content.text = _content;
    }
}
