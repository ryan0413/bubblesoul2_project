﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleRotate : MonoBehaviour
{
    public enum RotateAxis { X, Y, Z }
    public RotateAxis rotateAxis = RotateAxis.Y;

    public float rotationSpeed;

    private Transform m_Transform;

    private void Awake()
    {
        m_Transform = this.GetComponent<Transform>();
    }

    private void Update()
    {
        switch (rotateAxis)
        {
            case RotateAxis.X:
                m_Transform.Rotate(new Vector3(rotationSpeed * Time.deltaTime, 0, 0));
                break;
            case RotateAxis.Y:
                m_Transform.Rotate(new Vector3(0, rotationSpeed * Time.deltaTime, 0));
                break;
            case RotateAxis.Z:
                m_Transform.Rotate(new Vector3(0, 0, rotationSpeed * Time.deltaTime));
                break;
        }
    }
}
